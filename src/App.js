import React, { Component } from 'react';

import Comments from './Comments';
import NewComment from './NewComment';

class App extends Component {
  state = {
    newComment: 'valor',
    comments: [
      'Comment 1',
      'Comment 2',
      'Comment 3',
      'Comment 4',
    ]
  }

  sendComment = () => {
    this.setState({
      comments: [...this.state.comments, this.state.newComment],
      newComment: ''
    })
  }

  handleChange = event => {
      this.setState({
        newComment: event.target.value
      })
  }

  render() {
    return (
      <div>
        <NewComment
          newComment={this.state.newComment}
          sendComment={this.sendComment}
          handleChange={this.handleChange}
        />

        <Comments comments={this.state.comments} />
      </div>
    );
  }
}

export default App;
