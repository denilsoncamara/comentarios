import React, { Component } from 'react';

class NewComment extends Component {
  render() {
    return (
      <div>
        <textarea value={this.props.newComment} onChange={this.props.handleChange}></textarea>
        <button onClick={this.props.sendComment}>Enviar</button>
      </div>
    )
  }
}

export default NewComment;